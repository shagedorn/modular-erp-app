# Modular ERP App: Description & copyright/warranty notice #

## INFO ##

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and CocoaPods. The
repository is online for reference only.

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

This project demonstrates the development of a modular iOS application, starting from a
typical single-project app which can be found in the '[T1 ERP App](https://bitbucket.org/shagedorn/t1-erp-app)' 
repository.  The development process is described in an assignment thesis which was written 
at TU Dresden/SALT Solutions in 2012. The document has been published and is available
as a [free download](http://www.hagi-dd.de/docs/Belegarbeit.pdf) (German only).

**Tags/Milestones:**

+ **T1-ERP-APP**
	+ Starting point, basic ERP app for example tenant *T1*

  
+ [branch]-**ERP-APP-TARGET**
	+ Added branches/targets for two more example tenants and a branch/target for general development
	+ Resources and layout information are tenant-specific and read from configuration files

  	
+ [branch]-**ERP-APP-ACYCLIC-MODEL**
	+ Resolved inter-modular cyclic dependencies in model classes

  	
+ [branch]-**ERP-APP-INFRASTRUCTURE**
	+ Moved a general-purpose class to the infrastructure directory

  	
+ [branch]-**ERP-APP-MODULES**
	+ Created Xcode subprojects for all major directories/modules
	+ Adding/removing a module requires an entry in the 'ActiveModules'-PLIST and
		correct references in the 'Link Binary with Libraries" build phase
	+ Module base classes are *not* imported but loaded dynamically

  	
+ [branch]-**ERP-APP-SUBMODULES**
	+ New repositories have been created for all modules
		+ Consult the .gitmodules file for the location of the remote repositories
	+ Modules are loaded into the main project using Git Submodules
	+ Checkout of the project requires an update of all submodules
	+ Special tag: **T2-ERP-APP-CUSTOMER-ONLY**
		+ Only the Customer module is used on the *t2* branch

  	
+ [branch]-**ERP-APP-SB**
	+ stringsbuilder is used to combine string resources of all modules to one global
		.strings file during every build
		+ See https://bitbucket.org/shagedorn/stringsbuilder for the stringsbuilder project
	+ Define the strings that are needed in the 'TenantSpecificStrings'-PLIST
		+ It is not synced with 'ActiveModules.plist' automatically, but you should
		make sure they match
	+Special tag: **T2-ERP-APP-DE**
		+ German localisation added
		+ Some English translations have been changed for *T2* only

  		
+ [branch]-**ERP-APP-PODS**
	+ CocoaPods is used to resolve dependencies to open source libraries
		+ See http://cocoapods.org for the CocoaPods project – it needs to be installed
			separately and is not part of this project
	+ Every module needs to define the required Pods in 'ModuleDescription.plist'
	+ combinepods is used to create one global Podfile
		+ See https://bitbucket.org/shagedorn/combinepods for the combinepods project
	+ After a checkout or updates to the Pods requirements, you need to run the
		'RunPodsUpdate' target manually – it will create a new global Podfile and update
		the Pods project
		+ Running this target automatically during every build is not possible as the
			Pods update requires Xcode to reload the project (Click 'Revert' when prompted)
	+ Note: This version does not allow Archive builds – a fix was added to the project
		later ([branch]-ERP-APP-UPDATES)
	+ Special tag: **T3-ERP-APP-JSON**
		+ On the *t3* branch, JSON is used for the Order module's server data
			+ This requires the use of the SBJson library which is added using CocoaPods
	+ Special tag: **T3-ERP-APP-DATE**
		+ On the *t3* branch, every order also has a date attribute – a	generic field 
			was added to the data model on every branch, but only on *t3*, there
			is an interface to access it
			
  
+ [branch]-**ERP-APP-UPDATES**
	+ Visual updates have been introduced to the Article and Infrastructure modules
		to demonstrate the pros and cons of the new project structure
	+ In the super project/repository, only the submodules' commits needed to be updated
		
  
+ [branch]-**ERP-APP-QR**
	+ Another OS library has been added to the order module to demonstrate the use of
		CocoaPods in this project
	+ Support for appledoc: Generate documentation using the 'UpdateAppleDoc' target
		+ appledoc needs to be installed separately: http://gentlebytes.com/appledoc/
  		
  
**Runs on:** iOS 6 (iPad only)  
**IDE used:** Xcode 4.5

## LEGAL ##

### COPYRIGHT & WARRANTY NOTICE (Modular ERP App) ###

Modular ERP App was written by Sebastian Hagedorn as part of an assignment thesis at
TU Dresden/SALT Solutions GmbH. It was published under the following license (FreeBSD):

> Copyright (c) 2012 Sebastian Hagedorn
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
> 
> 1. Redistributions of source code must retain the above copyright notice, this
> list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice, this
> list of conditions and the following disclaimer in the documentation and/or other
> materials provided with the distribution.
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
> BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
> OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
> THE POSSIBILITY OF SUCH DAMAGE.

### COPYRIGHT & WARRANTY NOTICE (AFNetworking) ###

This software (Modular ERP App) includes the AFNetworking library which was published
under the following license (MIT):

> Copyright (c) 2011 Gowalla (http://gowalla.com/)
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in
> all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
> THE SOFTWARE.

### COPYRIGHT & WARRANTY NOTICE (SBJson) ###

This software (Modular ERP App) includes the SBJson library which was published
under the following license (BSD New):

> Copyright (C) 2007-2011 Stig Brautaset. All rights reserved.
>
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
>
> 1. Redistributions of source code must retain the above copyright notice, 
> this list of conditions and the following disclaimer.
>
> 2. Redistributions in binary form must reproduce the above copyright notice,
> this list of conditions and the following disclaimer in the documentation and/or
> other materials provided with the distribution.
>
> 3. Neither the name of the author nor the names of its contributors may be used
> to endorse or promote products derived from this software without specific 
> prior written permission.
>
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
> AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
> NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
> OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
> OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
> ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### COPYRIGHT & WARRANTY NOTICE (iOS-QR-Code-Encoder) ###

This software (Modular ERP App) includes the iOS-QR-Code-Encoder library which was published
under the following license (MIT):

> QR Code Generator - generates UIImage from NSString
> 
> Copyright (C) 2012 http://moqod.com Andrew Kopanev <andrew@moqod.com>
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the "Software"), to deal in
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
> of the Software, and to permit persons to whom the Software is furnished to do so, 
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
> INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
> PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
> FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
> OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
> DEALINGS IN THE SOFTWARE.