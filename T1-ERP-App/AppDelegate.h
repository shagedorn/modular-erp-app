//
//  AppDelegate.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 5/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <InfrastructureModule/CoreDataAppDelegate.h>

/**
 *  The app delegate initialises the persistent object store,
 *  the managed object context, applies custom UI styling
 *  (see GUIStyle class reference) and brings up the
 *  StartScreenController.
 */
@interface AppDelegate : UIResponder <CoreDataAppDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
