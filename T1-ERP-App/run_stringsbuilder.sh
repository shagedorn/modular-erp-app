#!/bin/sh

#  run_stringsbuilder.sh
#  Modular ERP App
#
#  Created by Sebastian Hagedorn on 18/11/12.
#  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.


#####

# The content of the Localizable.strings file is automatically generated
# during each build process using stringsbuilder.

# Get stringsbuilder: https://bitbucket.org/shagedorn/stringsbuilder
# The binary is distributed with this project.

#####

$SOURCE_ROOT/Tools/stringsbuilder/stringsbuilder $SOURCE_ROOT/T1-ERP-App Resources/BaseStrings.plist Tenant/TenantSpecificStrings.plist Resources 30 4 Localizable